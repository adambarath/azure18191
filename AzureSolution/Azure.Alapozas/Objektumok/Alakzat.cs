﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Azure.Alapozas.Objektumok
{
    /// <summary>
    /// Absztrakt osztály:
    /// - nem példányosítható
    /// - lehetnek absztakt függvényei és metódusai és etc., 
    ///     amik abstract kulcsszóval vannak ellátva és a 
    ///     leszármazottaknak kötelező megvalósítani ezeket.
    /// - abstract kulcsszóval ellátott tagok nem rendelkeznek törzzsel ( '{...}')
    /// </summary>
    public abstract class Alakzat // : object -> minden ősattya az OBJECT ősosztály
    {
        public abstract double Terület();

        public abstract double Kerület();

        public override string ToString()
        {
            string szoveg = $"Type: {GetType().Name} T:{Terület()} - K:{Kerület()}";
            return szoveg;
        }
    }
}
