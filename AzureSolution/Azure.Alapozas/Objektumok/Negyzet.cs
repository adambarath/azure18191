﻿using System;

namespace Azure.Alapozas.Objektumok
{
    public class Negyzet : Alakzat
    {
        public double A;

        public override double Kerület()
        {
            return 4 * A;
        }

        public override double Terület()
        {
            return A * A;
        }
    }
}
