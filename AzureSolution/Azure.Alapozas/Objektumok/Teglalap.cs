﻿using System;

namespace Azure.Alapozas.Objektumok
{
    public class Teglalap : Alakzat
    {
        public double A;
        public double B;

        public override double Kerület()
        {
            return 2 * (A + B);
        }

        public override double Terület()
        {
            return A * B;
        }
    }
}
