﻿using System;

namespace Azure.Alapozas.Objektumok
{
    public class Kör : Alakzat
    {
        // sugár
        public double R;

        public override double Kerület()
        {
            return 2 * this.R * Math.PI;
        }

        public override double Terület()
        {
            return this.R * this.R * Math.PI;
        }
    }
}
