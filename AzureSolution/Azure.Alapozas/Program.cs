﻿using System;
using Azure.Alapozas.Objektumok;
using System.Collections.Generic;

namespace Azure.Alapozas
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Hello world!"); // névtér alapú osztály hivatkozás // ~FQDN

            #region OOP

            RunOopNewInstances_Abstraction();
            RunOopNewInstances_Polymorphism();
            RunOopNewInstances_ReferenceVsValueTypes();

            #endregion

            #region Váltzozók:

            //DateTime.Now.AddDays(1);

            //DateTime dateTime = new DateTime(2012, 02, 28);
            //DateTime tomorrow = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day + 1);

            string szoveg = "hello ez egy szoveg!";
            Console.WriteLine(szoveg);

            // változók definiálása
            short piciSzam = 5;
            int egesz = 21111111;
            long nagyEgeszSzam = 999999999999;

            decimal penzugyiAlkalmazasokhoz = 5m;

            double lebegoPontos = 0.0000005;

            double lebegoPontos2 = 2d; // 2.00000000000000000000000000000000000
            double lebegoPontos3 = 2d; // 2.00000000000000000000000000000000000

            bool igazHamis = true; // false

            #endregion

            #region elágazások:

            if (Math.Abs(lebegoPontos2 - lebegoPontos3) < double.Epsilon)
            {
                Console.WriteLine("Ezek bizony egyenlőek!");
            }

            igazHamis = DateTime.Now != DateTime.Now.AddDays(-1); // természetesen igaz lesz

            if (true) Console.WriteLine("Ez igaz!"); // egy SOROS feltétel
            ;//üres utasítás, csak eszi a processzor időt.

            if (false) ;        // egy soros utasítás, ami lezárja a feltétel és az ez utáni blokkbe belefut
            {
                Console.WriteLine("Pedig ide nem kéne");
            }

            if (igazHamis)
            {
                Console.WriteLine("Ide nem fut be! Mert a paraméter az hamis értékű");
            }
            else
            {
                Console.WriteLine("Ide nem fut be!");
            }

            switch (piciSzam)   // ennek a piciSzam változónak az értéke 5
            {
                case 1:
                    Console.WriteLine("piciSzam változónak az értéke 1");
                    break;
                case 2:
                    Console.WriteLine("piciSzam változónak az értéke 2");
                    break;
                case 5:
                    Console.WriteLine("piciSzam változónak az értéke 5");
                    break;
                default:
                    Console.WriteLine("Egyik sem");
                    break;
            }

            switch (piciSzam)   // ennek a piciSzam változónak az értéke 5
            {
                case 1:
                case 2:
                    Console.WriteLine("piciSzam változónak az értéke NEM 5");
                    break;
                case 5:
                    Console.WriteLine("piciSzam változónak az értéke 5");
                    break;
                default:
                    Console.WriteLine("Egyik sem");
                    break;
            }

            #endregion

            #region ciklusok:
            // for, foreach, while, do while,

            // 3 paraméter: 1. ciklusváltozó, 2. ciklus feltétel, 3. ciklus léptetés utasítás
            for (int i = 0; i < 5; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine("Végtelen ciklus CONTINUE előtt;");
                    continue;   // tovább léptet
                }
                Console.WriteLine("Végtelen ciklus CONTINUE után;");

                // ciklus mag
                Console.WriteLine("Ciklus lépés: " + i); // string összefűzés
            }

            // végtelen ciklus (ha nem lenne ott a break)
            for (; ; )  // nincs ciklus feltétel, nem fog megállni soha
            {
                break; // ciklus megszakítás
            }
            Console.WriteLine("Végtelen ciklus BREAK után;");

            string[] collection = new string[3] { "egy", "ketto", "harom" };

            for (int i = 0; i < collection.Length; i++)
            {
                Console.WriteLine($"\"foreach\" item={collection[i]}"); // string interpolation
            }

            foreach (string egySzovegAHalmazbol in collection)
            {
                Console.WriteLine($"\"foreach\" item={egySzovegAHalmazbol}"); // string interpolation
            }

            // foreach hogy is működik
            var enumerator = collection.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine($"\"foreach\" item={enumerator.Current}"); // string interpolation
            }

            var enumeratorForFor = collection.GetEnumerator();
            for (
                string egySzovegAHalmazbol = null;
                enumeratorForFor.MoveNext();
                egySzovegAHalmazbol = (string)enumeratorForFor.Current
                )
            {
                Console.WriteLine($"\"foreach\" item={egySzovegAHalmazbol}"); // string interpolation
            }

            #endregion

            #region metódusok, függvények

            var eztFogjaKiírni = "kakukk";
            IrdKiAmiAParameterbenVan(eztFogjaKiírni, 2);

            string eztCsinalta = FuzdOsszeEnnyiszer(eztFogjaKiírni, 25);
            Console.WriteLine(eztCsinalta);

            Console.WriteLine(FuzdOsszeEnnyiszer(eztFogjaKiírni, 1));   // nem kell változóba menteni a visszatérési értéket

            #endregion

            Console.ReadKey();
        }

        /// <summary>
        /// Ki írja az argumentumot
        /// </summary>
        /// <param name="eztIrdKi">ennek a változónak az értékét írja majd ki</param>
        static void IrdKiAmiAParameterbenVan(string eztIrdKi, int ennyiszer) // lehet üres is a parameter lista
        {
            for (int i = 0; i < ennyiszer; i++)
            {
                Console.WriteLine(eztIrdKi);
            }
        }

        /// <summary>
        /// Megsokszoroz egy string-et
        /// </summary>
        /// <param name="eztFuzzukOssze">egy szöveg amit valahányszor meg akarunk sokszorozni</param>
        /// <returns>sokszorosított string</returns>
        static string FuzdOsszeEnnyiszer(string eztFuzzukOssze, int ennyiszer)
        {
            string eredmeny = string.Empty; // string.Empty == ""
            for (int i = 0; i < ennyiszer; i++)
            {
                eredmeny = eredmeny + eztFuzzukOssze;
            }

            return eredmeny;
        }

        static void RunOopNewInstances_Abstraction()
        {
            // PéldányosítGATÁS
            //int i = new int();
            //int j = 0;

            Kör alakzat1 = new Kör();
            Kör alakzat2 = new Kör();

            //double egyValtozo = 3;
            //alakzat1.R = egyValtozo;// ilyenkor a változó érték a R-be másolódik
            alakzat1.R = 2.0; // vagy 2d
            alakzat2.R = 5.6;

            Console.WriteLine(alakzat1.ToString());
            Console.WriteLine(alakzat2.ToString());

            Negyzet alakzat3 = new Negyzet();
            alakzat3.A = 4d;

            Console.WriteLine(alakzat3.ToString());

            Teglalap alakzat4 = new Teglalap();
            alakzat4.A = 4d;
            alakzat4.B = 5d;

            Console.WriteLine(alakzat4.ToString());

        }

        static void RunOopNewInstances_Polymorphism()
        {
            Kör alakzat1 = new Kör();
            alakzat1.R = 5.6;

            Negyzet alakzat2 = new Negyzet();
            alakzat2.A = 4d;

            Teglalap alakzat3 = new Teglalap();
            alakzat3.A = 4d;
            alakzat3.B = 5d;

            // feltöltjük a generikus listánkat Alakzat leszármazottakkal:
            List<Alakzat> altalanosListaAzAlakzatoknak = new List<Alakzat>();
            altalanosListaAzAlakzatoknak.Add(alakzat1);
            altalanosListaAzAlakzatoknak.Add(alakzat2);
            altalanosListaAzAlakzatoknak.Add(alakzat3);

            foreach (Alakzat egyAlakzat in altalanosListaAzAlakzatoknak)
            {
                // feladat: minden alakzat Kerületét kétszerezzük meg!

                // típus vizsgálat!!! 'is' operator
                if (egyAlakzat is Kör) // ilyet már nem cisnálunk: (egyAlakzat.GetType() == typeof(Kör) )
                {
                    // CAST-olás típus a zárójelekben
                    // castoltuk és kimentettük egy változóba
                    // !!! nem új példány jött létre, hanem csak egy referencia az eredeti objektumról!!!
                    // ebben az esetben az alakzat egy referenciára hivatkozik majd!!!
                    Kör egyKör = (Kör)egyAlakzat;
                    egyKör.R = egyKör.R * 2;
                }

                if (egyAlakzat is Negyzet)
                {
                    Negyzet egynegyzet = (Negyzet)egyAlakzat;
                    egynegyzet.A = egynegyzet.A * 2;
                }



                if (egyAlakzat is Teglalap)
                {
                    Teglalap egyteglalap = (Teglalap)egyAlakzat;
                    egyteglalap.A = 2 * egyteglalap.A;
                    egyteglalap.B = 2 * egyteglalap.B;
                }

                Console.WriteLine(egyAlakzat.ToString());
            }
        }

        static void RunOopNewInstances_ReferenceVsValueTypes()
        {
            Console.WriteLine($"érték típusok:");
            int i = 5;
            Console.WriteLine($"i={i}");
            int b = i;                    // érték átadás
            Console.WriteLine($"b={b}");
            b = 10;           //b  érték módosul, nem az i
            Console.WriteLine($"b={b}");
            Console.WriteLine($"i={i}");

            Console.WriteLine($"referencia típusok #1:");
            Negyzet osztalyom1 = new Negyzet(); // memória terület foglalás, példányosítás
            osztalyom1.A = 2;
            Console.WriteLine($"osztalyom1.A={osztalyom1.A}");
            Negyzet osztalyom2 = osztalyom1;    // nem érték másolás, hanem refence átadás!!!
            Console.WriteLine($"osztalyom2.A={osztalyom2.A}");
            osztalyom2.A = 10;
            Console.WriteLine($"osztalyom2.A={osztalyom2.A}");
            Console.WriteLine($"osztalyom1.A={osztalyom1.A}");
        }
    }

    partial class PartialClassPelda
    {
        public int i; // mező
    }

    partial class PartialClassPelda
    {
        //public int i; // másik partial classban már létezik itt nem szabad redundánsan. fordító nem is engedi
    }
}
