﻿using Azure.StoreApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Azure.WebApp.Controllers
{
    public class TodoController : Controller
    {
        static readonly List<TodoItem> Items = new List<TodoItem>
        {
            new TodoItem() { Name = "kenyér", Description ="Bótba", Date = DateTime.Now },
            new TodoItem() { Name = "szalámi", Description ="Bótba", Date = DateTime.Now },
            new TodoItem() { Name = "tej", Description ="Bótba", Date = DateTime.Now },
        };

        // GET: Todo
        public ActionResult Index()
        {
            return View(Items);
        }

        // GET: Todo/Details/?
        public ActionResult Details(Guid id)
        {
            return View(Items.FirstOrDefault(x => x.Id == id));
        }

        // GET: Todo/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Todo/Create
        [HttpPost]
        public ActionResult Create(TodoItem item)
        {
            try
            {
                item.Date = DateTime.Now;
                Items.Add(item);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Todo/Edit/5
        public ActionResult Edit(Guid id)
        {
            return View(Items.FirstOrDefault(x => x.Id == id));
        }

        // POST: Todo/Edit/5
        [HttpPost]
        public ActionResult Edit(Guid id, TodoItem item)
        {
            try
            {
                TodoItem toEdit = Items.FirstOrDefault(x => x.Id == id);
                toEdit.Description = item.Description;
                toEdit.Name = item.Name;

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Todo/Delete/?
        public ActionResult Delete(Guid id)
        {
            return View(Items.FirstOrDefault(x => x.Id == id));
        }

        // POST: Todo/Delete/?
        [HttpPost]
        public ActionResult Delete(Guid id, FormCollection collection)
        {
            try
            {
                TodoItem itemToRemove = Items.FirstOrDefault(x => x.Id == id);
                Items.Remove(itemToRemove);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
