﻿using Azure.StoreApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Azure.StoreApp
{
    public sealed partial class AddTodoPage : Page
    {
        public TodoItem TodoItem { get; set; }

        public ObservableCollection<TodoItem> TodoItems { get; set; }


        public AddTodoPage()
        {
            TodoItem = new TodoItem() { Name = "kenyér" };
            TodoItems = new ObservableCollection<TodoItem>();

            InitializeComponent();
            // így is lehetne: BackButton.Click += BackButton_Click;
            Loaded += (s, e) =>
            {
                DataContext = TodoItem;
                TodoItemsListBox.ItemsSource = TodoItems;
            };
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            //var dialog = new MessageDialog(
            //    $" Neve: {TodoItem.Name}, \nLeirasa: {TodoItem.Description}, \nHatarido: {TodoItem.Date}");
            //await dialog.ShowAsync();

            TodoItems.Add(TodoItem);
        }
    }
}
