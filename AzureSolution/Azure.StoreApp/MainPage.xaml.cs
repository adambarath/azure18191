﻿using System;
using System.Diagnostics;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Azure.StoreApp
{

    public partial class MainPage : Page
    {
        //Grid MainGrid; ez biz a XAML-ben van.

        public MainPage()
        {
            this.InitializeComponent();
            Loaded += MainPage_Loaded;
        }

        private void MainPage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Button button = new Button();
            button.Content = "Új TODO elem létrehozása";
            button.Background = new SolidColorBrush(Colors.Blue);
            button.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
            button.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
            button.Click += Button_Click;

            Grid.SetColumn(button, 1);
            Grid.SetRow(button, 1);

            MainGrid.Children.Add(button);
        }

        private async void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            //MessageDialog dialog = new MessageDialog("Siker!");
            //await dialog.ShowAsync();

            Frame.Navigate(typeof(AddTodoPage));

            Debug.WriteLine("Dialog megjelenítve.");
        }
    }
}
